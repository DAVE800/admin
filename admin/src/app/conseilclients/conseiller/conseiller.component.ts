import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';

@Component({
  selector: 'app-conseiller',
  templateUrl: './conseiller.component.html',
  styleUrls: ['./conseiller.component.scss']
})
export class ConseillerComponent implements OnInit {
orders:any
  constructor(private monhttp:HttpService) { }

  ngOnInit(): void {
    
    this.monhttp.Aladin_orders().subscribe(
      res=>{
        if(res.data!=undefined){
          this.orders=res.data
          console.log(res)
        }
      
      },
      err=>{
        console.log(err)
      }

    )
  }



  logout(){
    localStorage.removeItem('role');
    location.href="/login"
  }

}
