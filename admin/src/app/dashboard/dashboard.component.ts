import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
declare var require:any;
var myalert=require('sweetalert2')
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  color: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  loading=false;
cust_counter=0;
text:any;
part:any;
cloth:any;
disp:any;
print:any;
gadget:any;
pack:any;
isclth="cloths";
isdisp="disps"
ispack="packs"
isgadg="gadgets";
isprint="prints"
order:any
pen=0;
ok=0;
new=0;
users:any

  constructor(private myhttp : HttpService) { }
  register={
    name:'',
    email:'',
    role:'',
    phone:'',
    lastname:''
  
  }

  ngOnInit(): void {
  let value:any
/*
  this.myhttp.getnew().subscribe(
    res=>{
      value=res;
      this.new=value.length;
    },
    err=>{
      console.log(err)
    });
  
  this.myhttp.getendedords().subscribe(
    res=>{
      value=res;
      this.ok=value.length;
    },
    err=>{
      console.log(err)
    });
    */
   this.getstaff()

  this.myhttp.getCustomers().subscribe(
    res=>{
      value=res;
      this.cust_counter=value.length;
    },
    err=>{
      console.log(err)
    });

this.myhttp.getPartners().subscribe(
  res=>{
    this.part=res.partner;
    console.log(this.part);
  },
  err=>{
    console.log(err);
  }
);

this.myhttp.getCloths().subscribe(
  res=>{
    console.log(res)
    this.cloth=res;
  },
  err=>{
    console.log(err);
  }
);

this.myhttp.getDisplays().subscribe(
  res=>{
    console.log(res)
    this.disp=res;
  },
  err=>{
    console.log(err);
  }
);

this.myhttp.getPrints().subscribe(
  res=>{
    console.log(res)
    this.print=res;
  },
  err=>{
    console.log(err);
  }
);

this.myhttp.getOrders().subscribe(
  res=>{
    this.order=res;
    console.log(this.order);
  },
  err=>{
    console.log(err);
  }
)

this.myhttp.getPacks().subscribe(
  res=>{
    console.log(res)
    this.pack=res;
  },
  err=>{
    console.log(err);
  }
);
/** *
this.myhttp.getPending().subscribe(
  res=>{
   value=res;
   this.pen=value.length;
  },
  err=>{
    console.log(err);
  }
)
**/

this.myhttp.getGadgets().subscribe(
  res=>{
    console.log(res)
    this.gadget=res;
  },
  err=>{
    console.log(err);
  }
);

  }


  Allowpartner(event:any){
    let id=event.target.id;
    this.myhttp.Updatepartner({activated:1},id).subscribe(
      res=> {
        console.log(res);
        location.reload();
      },
      err=>{
        console.log(err);
      }
    )

  }

  Allowproduct(event:any){
    let id=event.target.id;
    let title=event.target.title;
    console.log(event.target.title);
    if(title && id){
    this.myhttp.Updateproduct({is_ordered:1},id,title).subscribe(
  res=>{
    console.log(res);
    location.reload();
  },
  err=>{
    console.log(err);
  }
)
    }
   
   
  }


  
loaddetails(event:any){
  if(event.target.title && event.target.id){
    console.log(event.target.id,event.target.title);

  }
}
toggle(){
  this.loading=!this.loading;
}

choiserole(event:any){
 let role=event.target.value;
 this.register.role=role     
}
getstaff(){
  this.myhttp.getstaff().subscribe(
    res=>{
      this.users=res.users
      console.log(res)
    },
    err=>{
      console.log(err)
    }
  )
}
Register(){
  if(this.register.email!=""&&this.register.lastname!=""&&this.register.name&&this.register.phone!=""&&this.register.role!=""){
    this.toggle()
    this.myhttp.SaveUsers(
      {name:this.register.name + " " + this.register.lastname,email:this.register.email,phone:this.register.phone,role:this.register.role}).subscribe(
        res=>{
          console.log(res)
          if(res.data!=undefined){
            this.toggle()
            myalert.fire({
              title: "creation d'utilisateur",
            text: res.message,
            icon: "success",
             button: "Ok"
            })
          }else{
            this.toggle()
            myalert.fire({
              title: "Erreur",
            text: res.message,
            icon: "error",
             button: "Ok"
            })
          }
        },
        err=>{
          this.toggle()
          console.log(err);
        }
      )
    }
  }


}
