import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { fabric } from 'fabric';
declare var require:any;
var myalert=require('sweetalert2')
@Injectable({
  providedIn: 'root'
})
export class HttpService {
items:any;
contextMenuItems:any;

  constructor(private http:HttpClient) { 
    this.items = (() => {
      const fieldValue = localStorage.getItem('p');
      return fieldValue === null
        ? []
        : JSON.parse(fieldValue);
    })();
  }

 getCustomers():Observable<any>{
   let url="https://api.aladin.ci/users";
  return  this.http.get<any>(url)
  
 }

 getPartners():Observable<any>{
   let url = 'https://api.aladin.ci/users/partners';
   return this.http.get<any>(url)

 }

 getDisplays():Observable<any>{
   return this.http.get<any>('https://api.aladin.ci/partners/displays')
 }


 getCloths():Observable<any>{
  let url = 'https://api.aladin.ci/partners/cloths';
  return this.http.get<any>(url)

}

getPrints():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/partners/prints')
}

getOrders():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/orders/admin')
}


getGadgets():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/partners/gadgets')
}



getPacks():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/partners/packs')
}


Updatepartner(data:any,id:any):Observable<any>{
  let url ="https://api.aladin.ci/users/";

  return this.http.put<any>(url+id,data);
}

Updateproduct(data:any,id:any,title:any):Observable<any>{
  
  let url ="https://api.aladin.ci/";

  return this.http.put<any>(url+title + "/"+id, data);
}


getPending():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/orders/pending');

}

getendedords():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/orders/ok');

}


getnew():Observable<any>{
  return this.http.get<any>('https://api.aladin.ci/orders/new');

}

addText(value:any,fill:any,font:any,fontstyle:any):any{
let text = new fabric.IText(value,{
  top:150,
  left:200,
  fill:fill,
  fontStyle:fontstyle,
  fontSize:48,
  cornerStyle:'circle',
  fontFamily:font,
});
return text;

}

setTextBeforeEdit(text:any){
  if(text){
    if(text.underline){
      text.set({text:text._textBeforeEdit,underline:false})
    }else{
      text.set({text:text._textBeforeEdit})
    }
  }
}

removeText(canvas:any,text:any){
  canvas.remove(text)
}

getstaff():Observable<any>{
  let url ="https://api.aladin.ci/users/aladin/staff"
  return this.http.get<any>(url);

}


addImage(src:any):any{
  var img = new Image();
  img.src=src
  img.crossOrigin = 'anonymous';
  return img;
}


loadImage(img:any,mcolor:string="",canvas:any){
  img.onload = function() {
    var f_img = new fabric.Image(img);
     if(mcolor!="" && mcolor!=undefined){
  f_img.filters?.push(new fabric.Image.filters.BlendColor({
    color:mcolor, 
    mode: 'tint',
    alpha:0.7
    }));
   f_img.applyFilters()

}
 canvas.setBackgroundImage(f_img,canvas.renderAll.bind(canvas), {
  backgroundImageStretch: true,
  left:5,
  top:5,
  right:5,
  bottom:5,
  height:600,
  width:600,
});
canvas.centerObject(f_img)
}

}


setStrokeText(text:any,c:any){
  if(text){
    let color=c
    console.log(color)
    text.set({stroke:color});
  }

  
}
getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}
save () {
  localStorage.setItem("p",JSON.stringify(this.items));
}

add(dataset:any) {
  this.items.push(dataset);
  this.save();
}
  SaveUsers(data:any):Observable<any>{
    let url="https://api.aladin.ci/users/register"
    return this.http.post<any>(url,data);
  }
 LoginUsers(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/login"
  return this.http.post<any>(url,data);
 }

 //Service commande Conseiller client
 Aladin_orders():Observable<any>{
   let url="https://api.aladin.ci/orders/aladin/orders"
   return this.http.get<any>(url);
 }



 Insertdisp(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/disps"
  let form = new FormData()
 // formdata.append('name_cloths',data.name_cloths); 
//data=[[req.body.name,req.body.price,req.body.dim,req.body.is_ordered,date,"https://api.aladin.ci/disps/"+file.name,req.body.staff,req.body.description]]

  form.append('img',data.img,data.img.name);
  form.append('name',data.name_display);
  form.append('price',data.price);
  form.append('dim',data.dim);
  form.append('is_ordered',data.is_ordered);
  form.append('staff',data.staff);
  form.append('description',data.comment);

  return this.http.post<any>(url,form)
}

InsertdispImage(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/disps/images"
  let form = new FormData()
  form.append('disp',data.insertId)
  form.append('myfront',data.disp_fileone)
  form.append('myback',data.file3)
  form.append('mycback',data.file4)
  form.append('mycfront',data.disp_filetwo)
  return this.http.post<any>(url,form)

}


InsertText(data:any):Observable<any>
{
  let url="https://api.aladin.ci/users/texts";
  return this.http.post<any>(url,data)

}



Insertclothdata(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/cloths";
  let form = new FormData();

  form.append('img',data?.img,data?.img.name);
  form.append('name',data.name);
  form.append('price',data.price);
  form.append('type_imp',data.type);
  form.append('material',"cotton");
  form.append('size',data.size);
  form.append('description',data.description);
  form.append('staff',data.staff); 
  form.append('is_ordered',data.is_ordered);
  return this.http.post<any>(url,form);

}


Insertprintdata(data:any):Observable<any>{

  let url="https://api.aladin.ci/users/prints"
  let form = new FormData();
  console.log(data);
  let peli;
  if(data.typ_couv.peli!=""){
    peli=data.typ_couv.peli;
  }else{
     peli=data.typ_couv.verni
  }


  form.append('img',data.img)
  form.append('name',data.name_print)
  form.append('price',data.price);
  form.append('gram',data.gram)
  form.append('volet',data.volet)
  form.append('description',data.comment)
  form.append('staff',data.staff)
  form.append('is_ordered',data.is_ordered);
  form.append('pellicule',peli);

  return this.http.post<any>(url,form);

}


Insertgadgetdata(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/gadgets"
  let form =new FormData()
  form.append('img',data.img)
  form.append('name',data.name_gadget)
  form.append('price',data.price);
  form.append('description',data.comment)
  form.append('staff',data.staff)
  form.append('is_ordered',data.is_ordered);

  return this.http.post<any>(url,form)

}



Insertpackdata(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/packs"
  
  let form =new FormData()
  form.append('img',data.img);
  form.append('name',data.name_emb);
  form.append('price',data.price);
  form.append('description',data.comment);
  form.append('staff',data.staff);
  form.append('is_ordered',data.is_ordered);
  form.append('dim',data.dim);
  form.append('material',data.mat);

  return this.http.post<any>(url,form)
}

getcolor():Observable<any>{
  let url="https://api.aladin.ci/users/colors";
  return this.http.get<any>(url)

}


postcolor(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/colors"

 return this.http.post<any>(url,data)
}

InsertClothImage(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/cloths/images"
  let form = new FormData()
  form.append('disp',data.insertId)
  form.append('myfront',data.file1,data.file1.name)
  form.append('myback',data.file3,data.file3.name)
  form.append('mycback',data.file4,data.file4.name)
  form.append('mycfront',data.file2,data.file2.name)
  return this.http.post<any>(url,form)

}

InsertClothcolor(data:any):Observable<any>{
  let form = new FormData();
  let url ="https://api.aladin.ci/users/cloths/colors";
  form.append("img",data.img,data.img.name);
  form.append("link",data.link,data.link.name);
  form.append("insertedId",data.insertedId);
  form.append("lib",data.lib);
  return this.http.post<any>(url,form);
}

InsertpackImage(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/packs/images"
  let form = new FormData()
  form.append('disp',data.insertId)
  form.append('myfront',data.disp_fileone)
  form.append('myback',data.file3)
  form.append('mycback',data.file4)
  form.append('mycfront',data.disp_filetwo)
  return this.http.post<any>(url,form)


}


InsertprintImage(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/prints/images"
  let form = new FormData()
  form.append('disp',data.insertId)
  form.append('myfront',data.disp_fileone)
  form.append('myback',data.file3)
  form.append('mycback',data.file4)
  form.append('mycfront',data.disp_filetwo)
  return this.http.post<any>(url,form)


}

InsertgadgetImage(data:any):Observable<any>{
  let url="https://api.aladin.ci/users/gadgets/images"
  let form = new FormData()
  form.append('disp',data.insertId)
  form.append('myfront',data.disp_fileone)
  form.append('myback',data.file3)
  form.append('mycback',data.file4)
  form.append('mycfront',data.disp_filetwo)
  return this.http.post<any>(url,form);
}


triggerMouse(event:any){
  var evt = new MouseEvent("click", {
    view: window,
    bubbles: true,
    cancelable: true,
});
event.dispatchEvent(evt);

}

underLineText(text:any,click:boolean){
  if(text){
      text.set({underline:click})
  }
}


makebold(text:any,click:boolean){
  if(click){
    text.set({fontWeight:'bold'})
  }else{
    text.set({fontWeight:'normal'})

  }
}
makeitalic(text:any,click:boolean){
  if(click){
    text.set({fontStyle:'italic'})
  }else{
    text.set({fontStyle:'normal'})
  }

}


Copy(canvas:any) {
	// clone what are you copying since you
	// may want copy and paste on different moment.
	// and you do not want the changes happened
	// later to reflect on the copy.
	canvas.getActiveObject().clone(function(cloned:any) {
		canvas._clipboard= cloned;
	});
}
Paste(canvas:any) {
	// clone again, so you can do multiple copies.
	canvas._clipboard.clone(function(clonedObj:any) {
		canvas.discardActiveObject();
		clonedObj.set({
			left: clonedObj.left + 15,
			top: clonedObj.top + 15,
			evented: true,
		});
		if (clonedObj.type === 'activeSelection') {
			// active selection needs a reference to the canvas.
			clonedObj.canvas = canvas;
			clonedObj.forEachObject(function(obj:any) {
				canvas.add(obj);
			});
			// this should solve the unselectability
			clonedObj.setCoords();
		} else {
			canvas.add(clonedObj);
		}
		canvas._clipboard.top += 10;
		canvas._clipboard.left += 10;
		canvas.setActiveObject(clonedObj);
		canvas.requestRenderAll();
	});
}

handleChange(file:any) {
  var fileTypes = [".png"]; //The image format we need
  var filePath = file.name;
  let err:boolean=false
  if (filePath) {
               var filePic = file; //Selected file content--image
               var fileType = filePath.slice(filePath.indexOf(".")); //Select the format of the file
               var fileSize = file.size; //Select the file size
               if (fileTypes.indexOf(fileType) == -1) { //Determine whether the file format meets the requirements
                       myalert.fire({
                        title: "Erreur",
                        text: `le format ${fileType} n'est pas autorisé utilisez le format .png`,
                        icon: "error",
                         button: "Ok"
                        });
          return !err;
               }
      if (fileSize > 1024 * 1024) {
        myalert.fire({
          title: "Erreur",
          text: `les fichiers ne peuvent avoir une taille plus grand que 1MG!!`,
          icon: "error",
           button: "Ok"
          });
          return !err;
      }

      var reader = new FileReader();
      reader.readAsDataURL(filePic);
      reader.onload = function (e:any) {
          var data = e.target.result;
                       // Load the image to get the true width and height of the image
          var image = new Image();
          image.onload =  ():any=>{
              var width = image.width;
              var height = image.height;
                               if(width == 600 || height == 600) { 
                                 return err
                                                                     
              } else {
                   myalert.fire({
                    title: "Erreur",
                    text: `la taille doit etre ${600}x${600}`,
                    icon: "error",
                     button: "Ok"
                    });
                  return !err;
              }

          };
        };
  } else {

      return !err ;
      
  }

  return err
}

  

handleChanges(file:any):Boolean {
  var fileTypes = [".png"]; //The image format we need
  var filePath = file.name;
  let err:boolean=false
  if (filePath) {
               var filePic = file; //Selected file content--image
               var fileType = filePath.slice(filePath.indexOf(".")); //Select the format of the file
               var fileSize = file.size; //Select the file size
               if (fileTypes.indexOf(fileType) == -1) { //Determine whether the file format meets the requirements
                       myalert.fire({
                        title: "Erreur",
                        text: `le format ${fileType} n'est pas autorisé utilisez le format .png`,
                        icon: "error",
                         button: "Ok"
                        });
          return true;
               }
      if (fileSize > 1024 * 1024) {
        myalert.fire({
          title: "Erreur",
          text: `les fichiers ne peuvent avoir une taille plus grand que 1MG!!`,
          icon: "error",
           button: "Ok"
          });
          return true;
      }

      var reader = new FileReader();
      reader.readAsDataURL(filePic);
      reader.onload = function (e:any) {
          var data = e.target.result;
                       // Load the image to get the true width and height of the image
          var image = new Image();
          image.onload = function ():any {
              var width = image.width;
              var height = image.height;
                               if(width == 200 || height == 200) { //Judge file pixels
                                     
                                       
              } else {
                   myalert.fire({
                    title: "Erreur",
                    text: `la taille doit etre ${200}x${200}`,
                    icon: "error",
                     button: "Ok"
                    });
                  err=!err ;
              }


          };
        };
  } else {

      return !err ;
      
  }

  return err
}


saveShape(data:any):Observable<any>{

  let url="https://api.aladin.ci/shapes"

  return this.http.post<any>(url,data)

}

savefont(data:any):Observable<any>{

  let url="https://api.aladin.ci/fonts";
  
  return this.http.post<any>(url,data)

}

saveclipart(data:any):Observable<any>{

  let url="https://api.aladin.ci/cliparts"
  
  return this.http.post<any>(url,data)

}


getfonts():Observable<any>{
  let url="https://api.aladin.ci/fonts"
  
  return this.http.get<any>(url)

}


addStylesheetURL(url:any) {
  var link = document.createElement('link');
  link.rel = 'stylesheet';
  link.href = url;
  document.getElementsByTagName('head')[0].appendChild(link);

}


}










