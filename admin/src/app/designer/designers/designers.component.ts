import { Component, OnInit } from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
declare var require:any;
var myalert=require('sweetalert2')
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
import { ColorEvent } from 'ngx-color';
@Component({
  selector: 'app-designers',
  templateUrl: './designers.component.html',
  styleUrls: ['./designers.component.scss']
})
export class DesignersComponent implements OnInit {

show=true
size=25
color: ThemePalette = 'primary';
mode: ProgressSpinnerMode = 'indeterminate';
value = 50;
shape={
  name:""
}
clipart={
  name:""
}

fonts={
  name:"",
  url:""
}
state:any
loading=false;
  clothdata={
    name_cloth:"ddd",
    price:"",
    comment:"",
    mat_cloth:"",
    typ_imp:"",
    taille:"",
  is_ordered:1,
  };

disp_fileone:any;
disp_filetwo:any;
disp_image:any
mi:any;
file3:any;
file4:any;
arg3:any;
arg4:any;
insertId:any;
img_id:any;
isdisp=false;
isgadget=false;
isprint=false;
iscloth=true;
ispack=false;

dispdata={
  name_display:"nom affichage",
  price:0,
  comment:"description du produit .........",
  dim:0.5,
  is_ordered:1,
  
};
p=false;
t=false;
staff:any
printdata={
  name_print:"",
  price:0,
  comment:"ddd",
  gram:"",
  typ_pap:"",
  volet:"",
  format_print:"",
  typ_couv:{peli:"",
  verni:"",

  },
  is_ordered:1,

};
dispImgdata={


}
clothImgdata={}
printImgdata={}
embaImgdata={}
gadgImgdata={}

gadgetdata={
  name_gadget:"nom du gadget",
  price:0,
  comment:"comment",
  is_ordered:1,

};

embdata={
  name_emb:"sac",
  price:0,
  comment:"comment on",
  dim:"1553",
  mat:"",
  is_ordered:1,
};
hideform=false;
showform=true
file:any;
cardcolor="white";
listofcat=["Choisir categorie","Afficharge","Emballage","Gadget","Imprimé","Vetement"]
category:any;
colors=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"#800080","#808000","#000080"];


  constructor(private http:HttpService) { }

  ngOnInit(): void {
    this.staff = localStorage.getItem('user');
    console.log(this.staff)
    this.sectlectcategory();
    this.clothselect();
    this.gadselect();
    this.printselect();
    this.dispselect();

  }

  toggleform(){
    this.hideform=this.showform
    this.showform=!this.hideform
  } 

  changeComplete($event:ColorEvent){
    console.log($event.color.hex);
    this.cardcolor=$event.color.hex;
    this.card_color();
   
  }

  savecolor(){
  let data ={
    lib:this.cardcolor,
    staff:(+this.staff)
  }
  if(this.color?.indexOf(data.lib)==-1){
    if(data.lib.search("#")==0){
      this.toggle()
  
    this.http.postcolor(data).subscribe(
      res=>{
        this.toggle()
        console.log(res);
        myalert.fire({
          title: "Enregistrement",
          text: "couleur enregistrée",
          icon: "success",
           button: "Ok"
          });
          location.reload();
      },
      err=>{
        this.toggle();
        myalert.fire({
          title: "Erreur",
          text: "une erreur s'est produite",
          icon: "error",
           button: "Ok"
          })

        console.log(err)
      }
    )

    }else{
      myalert.fire({
        title: "Erreur",
        text: "mauvais format du code veuillez utiliser le format hexadecimal",
        icon: "error",
         button: "Ok"
        })
    }
   

  }else{
    myalert.fire({
      title: "Erreur",
      text: "la couleur choisi existe déjà",
      icon: "error",
       button: "Ok"
      })
  }

  }

  card_color(){
    document.getElementById('card')?.setAttribute("style","background:"+this.cardcolor+"!important")
    console.log(document.getElementById('card'));
  }

ondispfile1Upload(event:any){
 
  this.disp_fileone=event.target.files[0];
 if(!this.http.handleChange(this.disp_fileone)==true){

const reader = new FileReader();
reader.onload = () => {
this.disp_image = reader.result;

 };
 reader.readAsDataURL(this.disp_fileone);

 if(this.isdisp==true){

  Object.assign(this.dispImgdata,{disp_fileone:this.disp_fileone})

 }

 if(this.isgadget==true){
  Object.assign(this.gadgImgdata,{disp_fileone:this.disp_fileone})

 }

 if(this.isprint==true){
  Object.assign(this.printImgdata,{disp_fileone:this.disp_fileone})

 }

 if(this.ispack==true){
  Object.assign(this.embaImgdata,{disp_fileone:this.disp_fileone})

 }


 if(this.iscloth==true){
  Object.assign(this.clothImgdata,{disp_fileone:this.disp_fileone})

 }

  }
  

}

ondispfile2Upload(event:any){
  this.disp_filetwo=event.target.files[0];
  console.log(this.http.handleChange(this.disp_filetwo));
  
if(!this.http.handleChange(this.disp_filetwo)==true){
  
  console.log(this.disp_filetwo);
  const reader = new FileReader();
  reader.onload = () => {
   this.mi = reader.result;
   };

   reader.readAsDataURL(this.disp_filetwo);

   if(this.isdisp==true){
    Object.assign(this.dispdata,{img:this.disp_filetwo,staff:parseInt(this.staff)})
  
    Object.assign(this.dispImgdata,{disp_filetwo:this.disp_filetwo})
  
   }
  
   if(this.isgadget==true){
    Object.assign(this.gadgetdata,{img:this.disp_filetwo,staff:parseInt(this.staff)})
  
    Object.assign(this.gadgImgdata,{disp_filetwo:this.disp_filetwo})
  

   }
  
  
   if(this.iscloth==true){
    Object.assign(this.clothdata,{img:this.disp_filetwo,staff:parseInt(this.staff)});
    Object.assign(this.clothImgdata,{disp_filetwo:this.disp_filetwo});
  
   }
  
   if(this.isprint==true){
    Object.assign(this.printdata,{img:this.disp_filetwo,staff:parseInt(this.staff)})
  
    Object.assign(this.printImgdata,{disp_filetwo:this.disp_filetwo})
  
   }
   if(this.ispack==true){
    Object.assign(this.embdata,{img:this.disp_filetwo,staff:parseInt(this.staff)})
    Object.assign(this.embaImgdata,{disp_filetwo:this.disp_filetwo})
  
   }
}

}

onfileupload3(event:any){
  this.file3=event.target.files[0];
if(!this.http.handleChange(this.file3)==true){

  const reader = new FileReader();
reader.onload = () => {
 this.arg3 = reader.result;
 };
 reader.readAsDataURL(this.file3);
 if(this.isdisp==true){
  Object.assign(this.dispImgdata,{file3:this.file3})

 }

 if(this.isgadget==true){
  Object.assign(this.gadgImgdata,{file3:this.file3})

 }

 if(this.iscloth==true){
  Object.assign(this.clothImgdata,{file3:this.file3})

 }

 if(this.isprint==true){
  Object.assign(this.printImgdata,{file3:this.file3})

 }
 if(this.ispack==true){
  Object.assign(this.embaImgdata,{file3:this.file3})

 }
}
}


onfileupload4(event:any){
  this.file4=event.target.files[0];
if(!this.http.handleChange(this.file4)==true){
  const reader = new FileReader();
  reader.onload = () => {
   this.arg4 = reader.result;
   };
   reader.readAsDataURL(this.file4);
   if(this.isgadget==true){
    Object.assign(this.gadgImgdata,{file4:this.file4})
  
   }
  
   if(this.isdisp==true){
    Object.assign(this.dispImgdata,{file4:this.file4})
   }
  
   if(this.iscloth==true){
    Object.assign(this.clothImgdata,{file4:this.file4})
  
   }
  
   if(this.isprint==true){
    Object.assign(this.printImgdata,{file4:this.file4})
  
   }
   if(this.ispack==true){
    Object.assign(this.embaImgdata,{file4:this.file4})
  
   }
  
}
 
}


sectlectcategory(){
 
  var select5=document.getElementById("catpack");

  this.listofcat.forEach(function(font) {
  var opt = document.createElement('option');
  if(font=="Emballage"){
    opt.selected
   }
   opt.innerHTML=font;
   opt.value=font;
   select5?.appendChild(opt);

    });

}

toggle(){
  this.loading=!this.loading;
}

clothselect(){
  var select1 = document.getElementById("catcloth");

  this.listofcat.forEach(function(font) {
  var opt = document.createElement('option');
  if(font=="Vetement"){
    opt.selected
   }
   opt.innerHTML=font;
   opt.value=font;
   select1?.appendChild(opt);
  });

}


dispselect(){
  var select3=document.getElementById('catdisp'); 

  this.listofcat.forEach(function(font) {
  var opt = document.createElement('option');
  if(font=="Afficharge"){
   opt.selected
  }
   opt.innerHTML=font;
   opt.value=font;
   select3?.appendChild(opt);
  });

}


gadselect(){
  var select2=document.getElementById('catgad');

  this.listofcat.forEach(function(font) {
  var opt = document.createElement('option');
  if(font=="Gadget"){
    opt.selected
   }
   opt.innerHTML=font;
   opt.value=font;
  
   select2?.appendChild(opt);
  });

}



printselect(){
  var select4=document.getElementById('printcat');

  this.listofcat.forEach(function(font) {
  var opt = document.createElement('option');
  if(font=="Imprimé"){
    opt.selected
   }
   opt.innerHTML=font;
   opt.value=font;
   select4?.appendChild(opt);
  });

}


getcat(event:any){
  this.category=event.target.value
  if(this.category!=undefined){


    if(this.category==="Afficharge"){
      this.isdisp=true
      console.log(this.isdisp)

    }

    if(this.category==="Emballage"){
      this.ispack=true;
      console.log(this.ispack)

    
    }

    if(this.category==="Imprimé"){
      this.isprint=true;
      console.log(this.isprint)


    }

   
    if(this.category==="Gadget"){
      this.isgadget=true;
      console.log(this.isgadget); 
    }

  }
}

InsertdispImage(){
  if(this.isdisp==true){
    Object.assign(this.dispImgdata,{insertId:this.insertId});
    this.http.InsertdispImage(this.dispImgdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.toggle()
          this.img_id=res.data.insertId
          myalert.fire({
            title: "Enregistrement",
            text: res.message,
            icon: "success",
             button: "Ok"
            })
            var elt=document.getElementById('d');
            this.http.triggerMouse(elt)
  
        }else{
          this.toggle()
          myalert.fire({
          title: "Erreur",
          text: res.message,
          icon: "error",
           button: "Ok"
          })
        }
        
       
      },
      err=>{
        this.toggle()
        myalert.fire({
        title: "Erreur",
        text: "erreur d'enregistrement de l'image",
        icon: "error",
         button: "Ok"
        });
        console.log(err);
      }
    )
   }

}
InsertpackImage(){
  if(this.ispack==true){
    Object.assign(this.embaImgdata,{insertId:this.insertId});
    this.http.InsertpackImage(this.embaImgdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.toggle()
          this.img_id=res.data.insertId
          myalert.fire({
            title: "Enregistrement",
            text: res.message,
            icon: "success",
             button: "Ok"
            })
            var elt=document.getElementById('em')
            this.http.triggerMouse(elt)
  
        }else{
          this.toggle()
          myalert.fire({
          title: "Erreur",
          text: res.message,
          icon: "error",
           button: "Ok"
          })
        }
      },
      err=>{
        console.log(err);
      }
    )
   }

}


InsertgadgetImage(){
  if(this.isgadget==true){
    Object.assign(this.gadgImgdata,{insertId:this.insertId});
    this.http.InsertgadgetImage(this.gadgImgdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.toggle()
          this.img_id=res.data.insertId
          myalert.fire({
            title: "Enregistrement",
            text: res.message,
            icon: "success",
             button: "Ok"
            })
            var elt=document.getElementById('ga')
            this.http.triggerMouse(elt)
  
        }else{
          this.toggle()
          myalert.fire({
          title: "Erreur",
          text: res.message,
          icon: "error",
           button: "Ok"
          })
        }
      },
      err=>{
        console.log(err);
      }
    )
   }

}


InsertclothImage(){
  if(this.iscloth==true){
    Object.assign(this.clothImgdata,{insertId:this.insertId});
   
   }

}

handlechange(event:any){
  this.file=event.target.files[0]

}

InsertprintImage(){
  if(this.isprint==true){
    Object.assign(this.printImgdata,{insertId:this.insertId});
    this.http.InsertprintImage(this.printImgdata).subscribe(res=>{
        if(res.data!=undefined){
          this.img_id=res.data.insertId
          this.toggle()
          myalert.fire({
          title: "succès",
          text: res.message,
          icon: "success",
           button: "Ok"
          });
          var elt=document.getElementById('pr')
            this.http.triggerMouse(elt)
          
        }else{
          this.toggle()
        myalert.fire({
        title: "Erreur",
        text: res.message,
        icon: "error",
         button: "Ok"
        })

        }
    }
    
    ,
    err=>{
      console.log(err)
    })

  }
}

vernis(event:any){
  var mot=event.target.value
  if(mot=='Vernissage'){
    this.p=!this.p;
    if(this.t==true){
      this.t=!this.t
    }
  }
  if(mot=='Pelliculage'){
    this.t=!this.t;
    if(this.p==true){
      this.p=!this.p
    }
  }
}


Insertgadget(){
if(this.category=="Gadget"){
  this.toggle()

  this.http.Insertgadgetdata(this.gadgetdata).subscribe(
    res=>{
      if(res.data!=undefined){
        this.insertId=res.data.insertId
        this.InsertgadgetImage()
        console.log(res)
      }else{
        console.log(res)
        this.toggle()
        myalert.fire({
        title: "Erreur",
        text: res.massage,
        icon: "error",
         button: "Ok"
        })

      }

      },
    
err=>{
  console.log(err)
  this.toggle()
  myalert.fire({
  title: "Erreur",
  text: "erreur server verifiez votre connexion et reactualisez la page",
  icon: "error",
   button: "Ok"
  })}
  )
}
}

Insertpack(){
  if(this.category=="Emballage"){
    this.toggle()

    this.http.Insertpackdata(this.embdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.insertId=res.data.insertId
          this.InsertpackImage()
          console.log(res)
        }
        else{
          console.log(res)
          this.toggle()
          myalert.fire({
          title: "Erreur",
          text: res.massage,
          icon: "error",
           button: "Ok"
          })
  
        }
      },
  err=>{
    console.log(err)
    this.toggle()
    
  }
    )
  }
  }


Insertcloth(){
   
    console.log(this.clothdata);
    this.toggle();
    this.http.Insertclothdata(this.clothdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.insertId=res.data.insertId
          this.InsertclothImage()
          console.log(res)
        }
        else{
          this.toggle();
          console.log(res)
          myalert.fire({
          title: "Erreur",
          text: res.massage,
          icon: "error",
           button: "Ok"
          })
  
        }
      },
  err=>{
    console.log(err)
    this.toggle();
   // window.location.reload();
  }
    )
  }
  

Insertprint(){
  if(this.category=="Imprimé"){
    this.toggle()

    this.http.Insertprintdata(this.printdata).subscribe(
      res=>{
        if(res.data!=undefined){
          this.insertId=res.data.insertId
          this.InsertprintImage()
          console.log(res)
        }
        else{
          console.log(res)
          this.toggle()
          myalert.fire({
          title: "Erreur",
          text: res.massage,
          icon: "error",
           button: "Ok"
          })
  
        }
      },
  err=>{
    console.log(err)
    this.toggle()
    //window.location.reload();
  
  }
    )
  }
  }


Insertdisp(){
  if(this.category=="Afficharge"){
    console.log(this.dispdata);
    this.toggle()
   this.http.Insertdisp(this.dispdata).subscribe(
     res=>{
      if(res.data!=undefined){
        this.insertId=res.data.insertId
        console.log(res)
        this.InsertdispImage()
      }
      else{
        console.log(res)
        this.toggle()
        myalert.fire({
        title: "Erreur",
        text: "ERREUR BAD REQUEST!!!",
        icon: "error",
         button: "Ok"
        });

      }
       
     },
     err=>{
      console.log(err)
      this.toggle();
     // window.location.reload();

     }
   )

  }
}
  logout(){
    localStorage.removeItem('user')
    localStorage.removeItem('role')
    location.href="/login"
  }


saveshape(){
  if(this.file){
    let formdata= new FormData();
    if(!this.http.handleChanges(this.file)){
      this.toggle();
      formdata.append("url",this.file,this.file.name);
      formdata.append("name",this.shape.name);
      this.http.saveShape(formdata).
      subscribe(res=>{
        this.toggle();
        console.log(res);
        myalert.fire({
          title: "Enregistrement",
          text: res.message,
          icon: "success",
           button: "Ok"
          });
      },
      err=>{
        this.toggle();

        console.log(err);
      });
    }

  }
  
}

saveClipart(){
  if(this.file){
    let formdata= new FormData();
    console.log!(this.http.handleChanges(this.file))
    if(!this.http.handleChanges(this.file)){
      this.toggle();

      formdata.append("url",this.file,this.file.name);
      formdata.append("name",this.clipart.name);
      this.http.saveclipart(formdata).
      subscribe(res=>{
        this.toggle();
        myalert.fire({
          title: "Enregistrement",
          text: res.message,
          icon: "success",
           button: "Ok"
          });
        console.log(res);
      },
      err=>{
        this.toggle();
       console.log(err);
      });
    }
   

  }
}
  savefont(){
    let data = this.fonts;
    if(data.name && data.url){
      this.toggle();

      this.http.savefont(data).subscribe(res=>{
        console.log(res);
        myalert.fire({
          title: "Enregistrement",
          text: res.message,
          icon: "success",
           button: "Ok"
          });
        this.toggle();
      },
      err=>{
        this.toggle();
        console.log(err);
      })
    }

  }

}
