import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DesignersComponent } from './designers/designers.component';
import { DesignerRoutingModule } from './designer-routing.module';
import { EditorComponent } from './editor/editor.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ClothComponent } from './cloth/cloth.component';
import { ColorSketchModule } from 'ngx-color/sketch';
import { ColorCircleModule } from 'ngx-color/circle'; 
import { ColorPhotoshopModule } from 'ngx-color/photoshop'; 
import { ColorMaterialModule } from 'ngx-color/material';
import { ColorSliderModule } from 'ngx-color/slider';
import { ColorShadeModule } from 'ngx-color/shade';
import { GadgetsComponent } from './gadgets/gadgets.component'
@NgModule({
  declarations: [
    DesignersComponent,
    EditorComponent,
    ClothComponent,
    GadgetsComponent,
  ],
  imports: [
    CommonModule,
    DesignerRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    MatProgressSpinnerModule,
    ColorSketchModule,
    ColorCircleModule,
    ColorPhotoshopModule,
    ColorMaterialModule,
    ColorSliderModule,
    ColorShadeModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA]
})
export class DesignerModule { }
