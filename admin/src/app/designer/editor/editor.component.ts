import { Component, OnInit,OnChanges,Input,Output,EventEmitter} from '@angular/core';
import { HttpService } from 'src/app/services/http.service';
declare var require:any
import { fabric } from 'fabric';
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
var myalert=require('sweetalert2');
import { ColorEvent } from 'ngx-color';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.scss']
})
export class EditorComponent implements OnInit,OnChanges {
Text:any;
lastText:any
canvas:any;
file:any;
image:any;
Image:any;
logo:any;
click=false
strokecolor="#0B44A5"
Textcolor="#0B44A5";
@Input() disp_image:any;
@Input() dip:any;
@Input() name_prod:any;
@Input() arg3:any;
@Input() arg4:any;
@Input() img_id:any;

Iclick=false
Bclick=false
color: ThemePalette = 'primary';
mode: ProgressSpinnerMode = 'indeterminate';
value = 50;
loading=false;
colorarray=['#D9E3F0', '#F47373', '#697689', '#37D67A', '#2CCCE4', '#555555',"#D2691E",'#dce775', '#ff8a65', '#ba68c8',"blue","yellow","orange","blacK","red","indigo","green","brown","#800080","#808000","#000080"];

@Output() newItemEvent =new EventEmitter<string>();

fonts = ["roboto","poppins","cinzel","Festive","satisfy","pacifico","monoton","Oleo Script","Pinyon Script"];

  constructor(private http:HttpService) { 
  
  }


copy(){
  this.http.Copy(this.canvas);
  }

paste(){
  this.http.Paste(this.canvas);
}

ngOnChanges(){
  if(this.disp_image){
    this.Image=this.http.addImage(this.disp_image);
    this.http.loadImage(this.Image,"",this.canvas);
    localStorage.setItem('f','0');
  }else{
    console.log(this.disp_image);
  }
  
}


OnRightClick(event:any){
  let item =this.canvas.getActiveObject();
  if(item!=undefined){
  var elt=document.getElementById('xdv')
  this.http.triggerMouse(elt)   
  }

  return false
}

toggle(){
  this.loading=!this.loading
}

  ngOnInit(): void {
    this.sectlectfont();
  let colors;
    this.canvas = new fabric.Canvas('canvas',{ 
      hoverCursor: 'pointer',
      selection: true,
      selectionBorderColor: 'blue',  
      fireRightClick: true,  // <-- enable firing of right click events
    });
    
    this.canvas.filterBackend=new fabric.WebglFilterBackend();
    this.http.getcolor().subscribe(
      res=>{
        colors=res;
        for(let item of colors){
          if(this.colorarray.indexOf(item.lib)==-1){
            this.colorarray.push(item.lib);
          }
        }
      }
    )
  
  }
  

  onFileUpload(event:any){
    this.file=event.target.files[0];
    const reader = new FileReader();
    reader.onload = () => {
     this.image = reader.result;
     };
     reader.readAsDataURL(this.file);

  }

  sectlectfont(){
    let font:any=[];

    this.http.getfonts().subscribe(
      res=>{
        font=res
        if(font.status==200){
          for(let item of font.fonts){
            this.fonts.push(item.name);
            this.http.addStylesheetURL(item.url);
          }
          this.fonts.unshift('Times New Roman');
          var select = document.getElementById("family");
          this.fonts.forEach(function(font) {
          var opt = document.createElement('option');
           opt.innerHTML=font;
           opt.value=font;
           select?.appendChild(opt);
             });
        }
      },
      err=>{
        console.log(err)
      }
    )
   
  }

  createText(event:any){
      this.Text=this.http.addText("mon texte ici",this.Textcolor,"times new roman","normal");
      this.canvas.add(this.Text)
      this.canvas.setActiveObject(this.Text);
      this.canvas.renderAll(this.Text);
      this.Text.enterEditing();
     
    
  }


  setStroke($event:ColorEvent){
    let item=this.canvas.getActiveObject();
    if(item){
      this.http.setStrokeText(item,$event.color.hex);

    }
  }

  underlineText(event:any){
   let item= this.canvas.getActiveObject()
   console.log(item)
   this.http.underLineText(item,!this.click);
   this.click=!this.click

    

  }
  
makeItalic(){
  let item= this.canvas.getActiveObject();
  this.http.makeitalic(item,!this.Iclick);
  this.Iclick=!this.Iclick;
}


makeBold(){
  let item= this.canvas.getActiveObject()
  this.http.makebold(item,!this.Bclick);
  this.Bclick=!this.Bclick

}


removeText(){
  let item= this.canvas.getActiveObject()
  this.http.removeText(this.canvas,item)
}


setTextBeforeEDit(){
  let item= this.canvas.getActiveObject()
  this.http.setTextBeforeEdit(item)
}


  changeFont(event:any){
    let item= this.canvas.getActiveObject()
    let font =event.target.value;
      item.set({fontFamily:font});
  }

getTextInfo(txt:any):any{
  let is_back:any= localStorage.getItem('f');
  let data={data:JSON.stringify(txt),back:+is_back,image:this.img_id};
  return data;

}

saveText(){
  let item = this.canvas.getActiveObject()
  this.getLogoInfo()
  if(item){
  let data=this.getTextInfo(item);
  this.toggle()
  this.http.InsertText(data).subscribe(
    res=>{
      if(res.data!=undefined){
        this.toggle();
        console.log(res);
        myalert.fire({
          title:"Enregistrement",
          text:res.message,
          icon:"success",
           button: "Ok"
          })      
      }else{
        this.toggle();
        console.log(res);
        myalert.fire({
        title:"Erreur",
        text:res.message,
        icon:"error",
         button: "Ok"
        })
      }
    },
    err=>{
      this.toggle();
      myalert.fire({
      title: "Erreur",
      text: "Une erreur s'est produite lors de l'enregistrement des informations",
      icon: "error",
       button: "Ok"
      })
      console.log(err);
    }
  );
  }
}

changeComplete($event:ColorEvent){
  console.log($event.color.hex);
  this.Textcolor=$event.color.hex;
  let text=this.canvas.getActiveObject();
  text.set({fill:this.Textcolor});


}
setLogo(event:any){
var imgInstance = new fabric.Image(event.target,{
  left: 200,
  top: 100,
  width:350,
  height:300
});
this.canvas.add(imgInstance).setActiveObject(imgInstance);
this.canvas.centerObject(imgInstance)
this.logo=imgInstance;
}

drawImage(src:any){
   document.getElementById('face1')?.setAttribute('src',src);
}

getLogoInfo(){
  let item:any = this.canvas.getActiveObject();
 // if(item.originalElement.src){
    console.log(item);
    
 // }
}


turnImage(){
  if(this.arg3){   
    this.drawImage(this.arg4);
    this.Image=this.http.addImage(this.arg3);
    this.http.loadImage(this.Image,"",this.canvas);
    localStorage.setItem('f','1');
    var objt=this.canvas.getObjects()
    for(let i=0;i<objt.length;i++){
     this.canvas.remove(objt[i]);
        }

  }else{
    console.log(this.arg3);
  }
 

}


customizeImage(){
  if(this.disp_image){
    this.drawImage(this.dip);
    this.Image=this.http.addImage(this.disp_image);
    this.http.loadImage(this.Image,"",this.canvas);
    localStorage.setItem('f','0');
    var objt=this.canvas.getObjects()
    for(let i=0;i<objt.length;i++){
     this.canvas.remove(objt[i]);
        }

  }

}



}
