import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { DesignersComponent } from './designers/designers.component';
import { EditorComponent } from './editor/editor.component';
import { DesignerGuard } from '../services/designer.guard';

const routes:Routes=[
  {
    path:'',component:DesignersComponent ,canActivate:[DesignerGuard]
},

{path:'editor',component:EditorComponent}

]

@NgModule({
  declarations: [],

  imports: [RouterModule.forChild(routes),CommonModule
  ],

})
export class DesignerRoutingModule { }
