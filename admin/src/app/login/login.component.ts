import { Component, OnInit } from '@angular/core';
import { HttpService } from '../services/http.service';
declare var require:any;
var myalert=require('sweetalert2')
import {ThemePalette} from '@angular/material/core';
import {ProgressSpinnerMode} from '@angular/material/progress-spinner';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  email:any;
  password:any;
  error=false;
  message=''
  color: ThemePalette = 'primary';
  mode: ProgressSpinnerMode = 'indeterminate';
  value = 50;
  loading=false;
  constructor(private loginservice:HttpService) {}

  ngOnInit(): void {
  }
  toggle(){
    this.loading=!this.loading
  }

  Login(){
    if(this.email!=null&&this.password!=null){
      this.toggle()
      this.loginservice.LoginUsers({email:this.email,password:this.password}).subscribe(
        res=>{
         if(res.user!=undefined){
          localStorage.setItem('user',res.user.id)
          localStorage.setItem('role',res.user.role)
          if(res.user.role=='admin'){
            location.href="/admin/dashboard"
          }
          if(res.user.role=='Designer'){
            location.href="/designers"
          }
          if(res.user.role =='Conseiller clients'){
            location.href="/customer/service"
          }
         }else{
           this.toggle()
           myalert.fire({
           title: "Erreur",
           text: res.message,
           icon: "error",
            button: "Ok"
           })
         }
        },
        err=>{
           this.toggle()
           myalert.fire({
           title: "Erreur",
           text: "mauvaise requete reactulisez la page",
           icon: "error",
            button: "Ok"
           })
          console.log(err)
        }
      )

    }else{
           myalert.fire({
           title: "Erreur",
           text: "Renseignez tous les champs svp",
           icon: "error",
            button: "Ok"
           })
    }
   
  }

}
