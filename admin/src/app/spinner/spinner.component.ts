import { Component, OnInit,Input,SimpleChange } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {
  @Input() size : number = 25;
  @Input() show: boolean =false;
  constructor() { }

  ngOnInit(): void {
  }
  ngOnChanges(changes:SimpleChange){
    console.log(changes); 
    
  }
}
