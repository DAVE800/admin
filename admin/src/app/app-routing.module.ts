import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {
    path:'',
    redirectTo: '/login',
    pathMatch: 'full',
},
  {path: 'admin', loadChildren:()=>import('./dashboard/dashboard.module').then(m=>m.DashboardModule)},
  {path: 'designers', loadChildren:()=>import('./designer/designer.module').then(m=>m.DesignerModule)},
 {path:'customer', loadChildren:()=>import('./conseilclients/conseilclients.module').then(t=>t.ConseilclientsModule
 )},
  {path: 'login', component: LoginComponent}
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
